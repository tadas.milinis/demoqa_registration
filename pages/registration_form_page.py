import time
import conftest
import os
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from pages.base_page import BasePage


class RegistrationForm(BasePage):
    url = "/registration/"

    def navigate(self):
        self.driver.get(conftest.domain + self.url)

    def submit(self):
        submit_btn = self.driver.find_element_by_xpath("//input[@name='pie_submit']")
        submit_btn.click()

    def set_field(self, label, value, field_type="input"):
        if field_type == "input":
            input_el = self.driver.find_element_by_xpath("//label[text()='{0}']/../input".format(label))
            input_el.clear()
            input_el.send_keys(value)
            input_el.send_keys(Keys.TAB)
        elif field_type == "textarea":
            input_el = self.driver.find_element_by_xpath("//label[text()='{0}']/../textarea".format(label))
            input_el.clear()
            input_el.send_keys(value)
        elif field_type == "radio":
            input_el = self.driver.find_element_by_xpath("//label[text()='{0}']/..//input[@value='{1}']".format(label, value))
            input_el.click()
        elif field_type == "checkbox":
            input_el = self.driver.find_element_by_xpath("//label[text()='{0}']/..//input[@value='{1}']".format(label, value))
            input_el.click()
        elif field_type == "dropdown":
            input_el = Select(self.driver.find_element_by_xpath("//label[text()='{0}']/../select".format(label)))
            input_el.select_by_value(value)
        elif field_type == "date":
            date_fields = self.driver.find_elements_by_xpath("//label[text()='{0}']/..//select".format(label))
            month_select = Select(date_fields[0])
            day_select = Select(date_fields[1])
            year_select = Select(date_fields[2])
            month, day, year = value.split("-")
            month_select.select_by_value(month)
            day_select.select_by_value(day)
            year_select.select_by_value(year)
        elif field_type == "file":
            upload_input = self.driver.find_element_by_xpath("//label[text()='{0}']/..//input[@type='file']".format(label))
            full_path = os.getcwd()+"//"+value
            upload_input.send_keys(full_path)
            self.driver.execute_script("$('#profile_pic_10').blur()")

    def set_password1(self, password):
        el = self.driver.find_element_by_id("password_2")
        el.clear()
        el.send_keys(password)
		# TODO: fix triggering password calculation
        time.sleep(0.5)
        el.send_keys(" ")
        el.send_keys(Keys.BACK_SPACE)
        el.send_keys(Keys.TAB)

    def set_password2(self, password):
        el = self.driver.find_element_by_id("confirm_password_password_2")
        el.clear()
        el.send_keys(password)
        el.send_keys(Keys.TAB)

    def set_password(self, password):
        self.set_password1(password)
        self.set_password2(password)

    def check_password_strength(self, expected_strength):
        strength = self.driver.find_element_by_id("piereg_passwordStrength")
        assert strength.text == expected_strength

    def check_validation_message(self, field_label, expected_validation):
        validation_message = self.driver.find_element_by_xpath("//label[text()='{0}']/ancestor::li//div[@class='legend_txt']".format(field_label))
        assert validation_message.text == expected_validation
