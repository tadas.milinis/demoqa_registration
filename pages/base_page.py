class BasePage(object):
    url = None

    def __init__(self, driver):
        self.driver = driver

    def check_if_page_contains(self, expected_text):
        assert (expected_text in self.driver.page_source)

    # some other common methods
