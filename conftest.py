import pytest
import os
from selenium import webdriver


domain = "http://demoqa.com"

@pytest.fixture()
def driver(request):
    print("Initiating driver")
    driver = webdriver.Chrome(executable_path="{0}//bin//chromedriver.exe".format(os.getcwd()))
    driver.get(domain)
    # driver.maximize_window()

    def close_driver():
        print("let's wrap it up")
        driver.quit

    request.addfinalizer(close_driver)
    return driver
