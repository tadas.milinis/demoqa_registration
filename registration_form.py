import pytest
import time
from pages.registration_form_page import RegistrationForm
from threading import Thread

ts = time.time()


@pytest.mark.usefixtures("driver")
class TestRegistrationFormSuite:

    def test_registration(self, driver):
        registration_form = RegistrationForm(driver)
        registration_form.navigate()

        registration_form.set_field("First Name", "Tadas")
        registration_form.set_field("Last Name", "Milinis")
        registration_form.set_field("Marital Status", "married", "radio")
        registration_form.set_field("Hobby", "reading", "checkbox")
        registration_form.set_field("Country", "Lithuania", "dropdown")
        registration_form.set_field("Date of Birth", "10-2-1981", "date")
        registration_form.set_field("Phone Number", "37068944444")
        registration_form.set_field("Username", "nafania{0}".format(ts))
        registration_form.set_field("E-mail", "nafania{0}@gmail.com".format(ts))
        registration_form.set_field("About Yourself", "Some fancy notes bout me!", "textarea")
        registration_form.set_field("Your Profile Picture", "pface.jpeg", "file")
        registration_form.set_field("Password", "Grybu!Karas?")
        registration_form.set_field("Confirm Password", "Grybu!Karas?")
        registration_form.submit()

        registration_form.check_if_page_contains("Thank you for your registration")

    def test_form_validation(self, driver):
        registration_form = RegistrationForm(driver)
        registration_form.navigate()

        # Mandatory fields
        registration_form.submit();
        registration_form.check_validation_message("First Name", "* This field is required")
        registration_form.check_validation_message("Hobby", "* This field is required")
        registration_form.check_validation_message("Phone Number", "* This field is required")
        registration_form.check_validation_message("Username", "* This field is required")
        registration_form.check_validation_message("E-mail", "* This field is required")
        registration_form.check_validation_message("Password", "* This field is required")
        registration_form.check_validation_message("Confirm Password", "* This field is required")

        # E-mail
        registration_form.set_field("E-mail", "wrongmail")
        registration_form.check_validation_message("E-mail", "* Invalid email address")

        # Phone number
        registration_form.set_field("Phone Number", "phone")
        registration_form.check_validation_message("Phone Number", "* Minimum 10 Digits starting with Country Code")
        registration_form.set_field("Phone Number", "123")
        registration_form.check_validation_message("Phone Number", "* Minimum 10 Digits starting with Country Code")

        # File
        registration_form.set_field("Your Profile Picture", "wrong_file.txt", "file")
        registration_form.check_validation_message("Your Profile Picture", "* Invalid File")

        # Passwords don't match
        registration_form.set_password1("password1")
        registration_form.set_password2("password2")
        registration_form.check_validation_message("Confirm Password", "* Fields do not match")
        registration_form.check_password_strength("Mismatch")

    def test_password_strength(self, driver):
        registration_form = RegistrationForm(driver)
        registration_form.navigate()

        # Too short
        registration_form.set_password("1234567")
        registration_form.check_validation_message("Password", "* Minimum 8 characters required")
        registration_form.check_password_strength("Very weak")

        # Very weak
        registration_form.set_password("aaaaaaaa")
        registration_form.check_password_strength("Very weak")

        # Weak
        registration_form.set_password("dundulis")
        registration_form.check_password_strength("Weak")

        # Medium
        registration_form.set_password("slaptas@125")
        registration_form.check_password_strength("Medium")

        # Strong
        registration_form.set_password("slaptas@12$$")
        registration_form.check_password_strength("Strong")
